Please follow below 4 steps to get things started:



1.git clone https://bitbucket.org/simonmh2u/megalon.git

2.cd megalon

3.mkvirtualenv megalon

4.pip install -r requirements.txt 



A.) On Command Line Running python weather_forecast.py <City Name> will return a Dict/Json with required specification (This concludes Part A bit in doc)





B.1.)python app.py (Listens on localhost:5000 which is a Rest Web Service)

Open browser and goto http://localhost:5000/weather/api/v1.0/forecast/<City Name> or

curl http://localhost:5000/weather/api/v1.0/forecast/<City Name> to get the JSON response in required format (This concludes B.1 in doc)

Note: "use the current date as the basis for calculatingthe next 2 weeks" not implemented in rest service as requirement not clear





B.2)In a different session  run ./manage.py runserver (listens on localhost:8000)

Open browser and goto (http://localhost:8000/registration/login) and login with user:simon pass:simon  (Frankly this bit of user registratio and login  was implemented by me couple of days back as part of a coding interview for a different company called Accilon -https://simonmh2u@bitbucket.org/simonmh2u/testme.git )



It takes you to index page after authentication where u see a button called ForeCast, click on it and the data is displayed	(This concludes B.2)

Note: I've not implemented parts of B.2. i.e calendar control, dropdown , tabular format etc . instead displyed data as regular para text as I did not have enough time

This is the reason City name  is hardcoded as London


