from django.conf.urls import patterns, include, url
from registration import views


urlpatterns = patterns('',
                       url(r'^index/$', 'registration.views.index', name='index'),
                       url(r'^register/$', 'registration.views.register', name='register'),
                       url(r'^login/$', 'registration.views.user_login', name='login'),
                       url(r'^logout/$', 'registration.views.user_logout', name='logout'),
                       )
