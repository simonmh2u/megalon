from collections import defaultdict
import sys, time
import requests


class Forecaster(object):
	def __init__(self,city):
		self.city = city

	def summarise_forecast(self):
		WEATHERURL='http://api.openweathermap.org/data/2.5/forecast/daily?q=%s&mode=json&units=metric&cnt=14' % self.city

		try:
			r =requests.get(WEATHERURL)
			rjson = r.json()
		except requests.exceptions.RequestException as e:
			print e
			sys.exit(1)

		data_tuple= [ (i['dt'],i['temp']['max'],i['temp']['min'],i['weather'][0]['main']) for i in rjson['list'] ]

		forecast_res = defaultdict(list)
		f1,f2=[],[]

		for datex,maxi,mini,weather_type in data_tuple:
	 		f1 += [maxi]
			f2 += [mini]
			forecast_res[weather_type].append(time.strftime('%Y-%m-%d', time.localtime(datex)))

		final_response={'max':max(f1),'min':min(f2),'forecast':dict(forecast_res),'city':self.city}
		return final_response


if __name__ == '__main__':
	f = Forecaster(sys.argv[1])
	print f.summarise_forecast()

