from django.test.client import Client
from django.test import TestCase
# Create your tests here.


class ViewTests(TestCase):

	def setUp(self):
		self.client = Client()

	def test_register(self):
		response = self.client.get('/registration/register/')
		self.assertEqual(response.status_code,200)
		self.assertContains(response, 'Register with Accilon')
		response = self.client.post('/registration/register/',{'username':'jack','password':'jack','email':'a@a.net','website':'http://simon.net','twitter':'@simno'})
		self.assertEqual(response.status_code,200)
		self.assertContains(response,'thank you for registering')