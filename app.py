from flask import Flask, jsonify
from flask_cors import CORS
from  weather_forecast import Forecaster

app = Flask(__name__)
cors = CORS(app)


@app.route('/weather/api/v1.0/forecast/<string:city>', methods=['GET'])
def get_tasks(city):
	weatherme = Forecaster(city)
	idumps = weatherme.summarise_forecast()
	return jsonify(idumps)

if __name__ == '__main__':
    app.run(debug=True)
